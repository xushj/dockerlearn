

PDF --> HTML 


备注： 有一定的局限性，对部分文档支持不是很好   （Java 3.pdf)


运行：

```Dockerfile
    
    docker run -ti --rm -v ~/pdf:/pdf bwits/pdf2htmlex-alpine pdf2htmlEX --zoom 1.3 test.pdf

```



参考：

[pdf2htmlex-alpine](https://hub.docker.com/r/bwits/pdf2htmlex-alpine/dockerfile)