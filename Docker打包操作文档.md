 2017.04.28  
```
--镜像：  https://github.com/docker-library 
参照：https://docs.docker.com/engine/reference/run/
```


1.  拉取alpine 镜像：(docker search alpine)

  ```
  docker pull alpine
```

2.在某一目录下：创建Dockerfile    可修改版本号 v3.4 改为v3.5 最新版本

```
# AlpineLinux open jre 8
FROM alpine:latest

# Install cURL
RUN echo -e "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.4/main\n\
https://mirror.tuna.tsinghua.edu.cn/alpine/v3.4/community" > /etc/apk/repositories

# Set time zoneinfo curl bash tree
RUN apk update && apk add curl bash tree tzdata \
    && cp -r -f /usr/share/zoneinfo/Hongkong /etc/localtime \
    && echo -ne "Alpine Linux 3.4 image. (`uname -rsv`)\n" >> /root/.built

```

3.进入到Dockerfile目录：
      
特别注意：如果包含.sh 等shell脚本，必须要赋权限   chmod + x   xxx.sh，不然运行失败。
```
编译新镜像：docker build -t alpine  .
备注：apline 可修改名称  --->如果不可修改，则运行两次即可   即：docker build -t alpine  .    和 docker build -t alpineChange  .
         .  表示当前位置

![输入图片说明](https://gitee.com/uploads/images/2018/0702/114922_41e2c566_727233.png "1.png")
```


!! 如果需要推送到阿里云服务上：注意名称 ＊＊

推送：
![输入图片说明](https://gitee.com/uploads/images/2018/0702/114940_04623a7c_727233.png "2.png")


4.运行alpine 镜像：

```
 docker run -itd --name alpine alpine /bin/bash   红色表示运行实例名称
```
![输入图片说明](https://gitee.com/uploads/images/2018/0702/114955_6dd340bd_727233.png "3.png")

5.进入服务：
```
 docker exec -it alpine /bin/bash   其中alpine 为服务名称
```
![输入图片说明](https://gitee.com/uploads/images/2018/0702/115020_5ee500c3_727233.png "4.png")

完成！

查看Docker安装目录：

which docker 

参数含义：

参照：https://docs.docker.com/engine/reference/run/