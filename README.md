# DockerLearn
学习Docker

Ubuntu
使用ubuntu作为基础镜像，需要先下载安装tzdata包，默认时区是UTC时区，修改配置文件，并通过dpkg-reconfigure重置时区配置生效。 安装完成之后，为了尽量减少镜像体力，删除安装过程中产生的各种非必要文件。

```dockerfile
FROM ubuntu
 
ENV TIME_ZONE Asia/Shanghai
 
RUN sed -i s@/archive.ubuntu.com/@/mirrors.aliyun.com/@g /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y tzdata \
    && ln -snf /usr/share/zoneinfo/$TIME_ZONE /etc/localtime && echo $TIME_ZONE > /etc/timezone \
    && dpkg-reconfigure -f noninteractive tzdata \
    && apt-get clean \
    && rm -rf /tmp/* /var/cache/* /usr/share/doc/* /usr/share/man/* /var/lib/apt/lists/*

```

Alphine
Alphine号称最小的Linux系统镜像（才5M), 先采用apk包管理器来安装tzdata包，设置相关配置文件。

```dockerfile
FROM alpine
MAINTAINER igitlib
 
RUN apk add --no-cache tzdata \
    && cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && echo "Asia/Shanghai" > /etc/timezone \
    && apk del tzdata

```
Centos
centos就相对很简单，只需要添加配置文件即可。

```dockerfile
FROM centos
 
ENV TIME_ZONE Asia/Shanghai
 
RUN ln -snf /usr/share/zoneinfo/$TIME_ZONE /etc/localtime
```

参考资料：

    https://www.docker.com/ 官网

    https://docs.docker.com/  docker文档 （https://docs.docker.com/engine/reference/run/）

    http://www.runoob.com/docker/docker-tutorial.html  菜鸟教程
    
    http://www.docker.org.cn/  Docker中文

    https://github.com/docker-library 镜像地址(https://github.com/docker-library/openjdk)

[设置时区](https://my.oschina.net/fastjrun/blog/4952286)